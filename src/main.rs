use warp::Filter;

#[tokio::main]
async fn main() {
    // GET /hello/warp => 200 OK with body "Hello, warp!"
    let get_temperature = warp::path!("api" / "forecast" / String)
        .map(|city| format!("{{\"city\": \"{}\", \"maxTemperature\": 31}}", city));

    let html_temperature = warp::path!("forecast" / String)
        .map(|city| format!("<html><h1>Forecast for {}</h1><body>Maximum temperature: 31</body></html>", city));


    warp::serve(get_temperature.or(html_temperature))
        .run(([0, 0, 0, 0], 3030))
        .await;
}